import { BrowserRouter, Routes, Route } from "react-router-dom";
import HomePage from "../pages/HomePage";
import React, { useEffect } from "react";
import MainLayout from "../layout";
import AboutPage from "../pages/AboutPage";
import ProductPage from "../pages/ProductPage";
import BlogPage from "../pages/BlogPage";
import ContactPage from "../pages/ContactPage";
import PaymentMethodsPage from "../pages/PaymentMethodsPage";
import ReturnsPage from "../pages/ReturnsPage";
import ShippingPage from "../pages/ShippingPage";
import PrivacyPolicyPage from "../pages/PrivacyPolicyPage";
import DashBoardPage from "../pages/DashBoardPage";
import CartPage from "../pages/CartPage";
import FaqPage from "../pages/FaqPage";
import Page404 from "../pages/Page404";
import PrivateRoute from "../components/PrivateRoute";
import CheckOutSuccessPage from "../pages/CheckOutSuccessPage";
import "./assets/style.css";
import ProductDetailPage from "../pages/ProductDetailPage";
import { useDispatch } from "react-redux";
import { message } from "antd";
import tokenMethod from "../utils/token";
import { handleGetProfile } from "../stores/reducer/authReducer";
import { handleGetCart } from "../stores/reducer/cartReducer";
import CheckOutPage from "../pages/CheckOutPage";

function App() {
  const dispatch = useDispatch();

  useEffect(() => {
    message.config({
      top: 80,
      duration: 3,
      maxCount: 3,
    });
    if (tokenMethod.get()) {
      dispatch(handleGetProfile());
      dispatch(handleGetCart());
    }
  });

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<MainLayout />}>
          <Route index element={<HomePage />} />
          <Route path="/about" element={<AboutPage />} />
          <Route path="/product" element={<ProductPage />} />
          <Route path="/products/:slug" element={<ProductDetailPage />} />
          <Route path="/blog" element={<BlogPage />} />
          <Route path="/faq" element={<FaqPage />} />
          <Route path="/contact" element={<ContactPage />} />
          <Route path="/payment-methods" element={<PaymentMethodsPage />} />
          <Route path="/returns" element={<ReturnsPage />} />
          <Route path="/shipping" element={<ShippingPage />} />
          <Route path="/privacy-policy" element={<PrivacyPolicyPage />} />

          <Route element={<PrivateRoute />}>
            <Route path="/dashboard" element={<DashBoardPage />} />
            <Route path="/cart" element={<CartPage />} />
            <Route path="/checkout" element={<CheckOutPage />} />
            <Route path="/checkout-success" element={<CheckOutSuccessPage />} />
          </Route>

          <Route path="*" element={<Page404 />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
