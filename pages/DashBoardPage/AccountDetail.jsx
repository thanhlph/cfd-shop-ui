import React, { useEffect, useState } from "react";
import Input from "../../components/Input";
import Select from "../../components/Select";
import { useAuthContext } from "../../context/AuthContext";
import useForm from "../../hooks/useForm";
import { regexRule, requireRule } from "../../utils/validate";

const rules = {
  firstName: [requireRule("Vui lòng nhập name")],
  email: [
    requireRule("Vui lòng nhập email"),
    regexRule("Vui lòng nhập đúng định dạng email", "email"),
  ],
  phone: [
    requireRule("Vui lòng nhập phone"),
    regexRule("Vui lòng nhập đúng định dạng phone", "phone"),
  ],
  password: [requireRule("Vui lòng nhập password")],
};
const AccountDetail = () => {
  // const { profile, province } = useAuthContext();
  // const [provinceId, setProvinceId] = useState("");

  const { email, phone } = profile || {};
  const provinceOptions =
    province?.length > 0
      ? [
          { value: "", type: "--" },
          ...province.map((province) => ({
            value: province.id,
            label: province.name,
          })),
        ]
      : [{ value: "", type: "--" }];

  const { form, setForm, register, validate } = useForm(
    {
      firstName: "",
      lastName: "",
      email: "",
      phone: "",
      facebookURL: "",
      website: "",
      introduce: "",
      birthday: "",
      street: "",
      province: "",
      district: "",
      ward: "",
    },
    {
      rules,
    }
  );

  useEffect(() => {}, []);

  const _handleChangeProvice = (e) => {
    setForm({ ...form, province: e.target.value });
  };
  return (
    <div
      className="tab-pane fade show active"
      id="tab-account"
      role="tabpanel"
      aria-labelledby="tab-account-link"
    >
      <form action="#" className="account-form">
        <div className="row">
          <div className="col-sm-6">
            <Input
              label="Full Name *"
              placeholder="Full Name"
              required
              {...register("firstName")}
            />
          </div>
          <div className="col-sm-6">
            <Input
              type="email"
              defaultValue={email || ""}
              label="Email address *"
              required
              disabled
              {...register("email")}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-sm-6">
            <Input
              defaultValue={phone || ""}
              label="Phone *"
              required
              {...register("phone")}
            />
          </div>
          <div className="col-sm-6">
            <label>Ngày sinh *</label>
            <input
              type="date"
              className="form-control"
              required
              {...register("birthday")}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-sm-4">
            <Input
              label="Province/City *"
              renderInput={(inputProps) => {
                return (
                  <Select
                    id="city"
                    options={provinceOptions}
                    {...inputProps}
                    onChange={(e) => _handleChangeProvice(e)}
                  />
                );
              }}
              {...register("province")}
            />
          </div>
          <div className="col-sm-4">
            <Input
              label="District *"
              renderInput={(inputProps) => {
                return <Select id="district" {...inputProps} />;
              }}
              {...register("district")}
            />
          </div>
          <div className="col-sm-4">
            <Input
              label="Ward *"
              renderInput={(inputProps) => {
                return <Select id="ward" {...inputProps} />;
              }}
              {...register("province")}
            />
          </div>
        </div>
        <Input label="Street address" {...register("street")} />
        <label>Current password (leave blank to leave unchanged)</label>
        <input type="password" className="form-control" />
        <label>New password (leave blank to leave unchanged)</label>
        <input type="password" className="form-control" />
        <label>Confirm new password</label>
        <input type="password" className="form-control mb-2" />
        <button type="submit" className="btn btn-outline-primary-2">
          <span>SAVE CHANGES</span>
          <i className="icon-long-arrow-right" />
        </button>
      </form>
    </div>
  );
};

export default AccountDetail;
