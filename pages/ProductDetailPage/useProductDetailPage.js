import { message } from "antd";
import queryString from "query-string";
import { useEffect, useRef } from "react";
import { useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import useMutation from "../../hooks/useMutation";
import { productService } from "../../services/productService";
import { handleAddCart } from "../../stores/reducer/cartReducer";

const useProductDetailPage = () => {
  const { slug } = useParams();
  const dispatch = useDispatch();
  const colorRef = useRef();
  const quantityRef = useRef();

  const { data: productDetailData, execute: fetchProductDetail } = useMutation(
    (slug) => productService.getProductBySlug(slug)
  );

  const { id, name, description, shippingReturn, price, discount } =
    productDetailData || {};
  const { data: productReviewData, execute: fetchProductReview } = useMutation(
    (id) => productService.getProductReview(id)
  );

  const productDetail = productDetailData;

  useEffect(() => {
    fetchProductDetail(slug);
    fetchProductReview(id);
  }, [slug, id]);

  const handleAddToCart = () => {
    const { value: color, reset: colorReset } = colorRef.current || {};
    const { value: quantity, reset: quantityReset } = quantityRef.current || {};

    if (!color) {
      message.error("Please select color");
      return;
    } else if (isNaN(quantity) && quantity < 1) {
      message.error("Quantity must be more than 0");
      return;
    }
    const addPayload = {
      addedId: id,
      addedColor: color,
      addedQuantity: quantity,
      addedPrice: price - discount,
    };
    try {
      const res = dispatch(handleAddCart(addPayload)).unwrap();
      if (res) {
        colorReset?.();
        quantityReset?.();
      }
    } catch (error) {
      console.log("error", error);
    }
  };

  const handleAddToWishlist = () => {
    console.log("Add To Wishlist");
  };

  const productDetailTopProps = {
    ...productDetail,
    reviews: productReviewData,
    colorRef,
    quantityRef,
    handleAddToCart,
    handleAddToWishlist,
  };

  const productDetailTabProps = {
    description,
    shippingReturn,
    reviews: productReviewData,
  };

  return { productName: name, productDetailTopProps, productDetailTabProps };
};
export default useProductDetailPage;
