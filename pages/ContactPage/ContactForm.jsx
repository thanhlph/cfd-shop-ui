import React from "react";
import Input from "../../components/Input";
import TextArea from "../../components/TextArea";

const ContactForm = () => {
  return (
    <form action="#" className="contact-form mb-3">
      <div className="row">
        <div className="col-sm-6">
          <Input required placeholder="Name *" />
        </div>
        <div className="col-sm-6">
          <Input required placeholder="Email *" />
        </div>
      </div>
      <div className="row">
        <div className="col-sm-6">
          <Input placeholder="Phone" />
        </div>
        <div className="col-sm-6">
          <Input placeholder="Subject" />
        </div>
      </div>
      <Input
        placeholder="Message *"
        required
        renderInput={(inputProps) => {
          return <TextArea {...inputProps} />;
        }}
      />
      <button
        type="submit"
        className="btn btn-outline-primary-2 btn-minwidth-sm"
      >
        <span>SUBMIT</span>
        <i className="icon-long-arrow-right" />
      </button>
    </form>
  );
};

export default ContactForm;
