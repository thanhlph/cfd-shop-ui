import classNames from "classnames";
import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import ProductCard from "../../components/ProductCard";
import owlCarousels from "../../utils/owlCarousels";

const FeaturedSection = ({
  categories,
  featureProducts,
  selectedCateSlug,
  handleSelectCate,
}) => {
  useEffect(() => {
    owlCarousels();
  }, [selectedCateSlug]);
  // const newCategories = [{ name: "All", slug: "all" }, ...categories];
  // console.log("newCategories :>> ", newCategories);
  const _onSelectCate = (e, slug) => {
    e.preventDefault();
    e.stopPropagation();
    handleSelectCate?.("");
    setTimeout(() => {
      handleSelectCate?.(slug);
    }, 200);
  };

  return (
    <div className="container top">
      <div className="heading heading-flex mb-3">
        <div className="heading-left">
          <h2 className="title">Featured Products</h2>
        </div>
        <div className="heading-right">
          <ul
            className="nav nav-pills nav-border-anim justify-content-center"
            role="tablist"
          >
            {/* <li className="nav-item">
              <a
                className="nav-link active"
                id="top-all-link"
                data-toggle="tab"
                href="#top-all-tab"
                role="tab"
                aria-controls="top-all-tab"
                aria-selected="true"
              >
                All
              </a>
            </li> */}
            {categories?.map((category) => {
              const { name, slug } = category || {};
              return (
                <li className="nav-item">
                  <a
                    className={classNames("nav-link", {
                      active: selectedCateSlug === slug,
                    })}
                    // id="top-tv-link"
                    // data-toggle="tab"
                    href="#top-tv-tab"
                    // role="tab"
                    // aria-controls="top-tv-tab"
                    // aria-selected="false"
                    onClick={(e) => _onSelectCate(e, slug)}
                  >
                    {name}
                  </a>
                </li>
              );
            })}
          </ul>
        </div>
      </div>
      <div className="tab-content tab-content-carousel just-action-icons-sm">
        <div
          className={classNames("tab-pane p-0 fade", {
            "show active": featureProducts?.length > 0,
          })}
          id="top-all-tab"
          role="tabpanel"
          aria-labelledby="top-all-link"
        >
          {featureProducts?.length > 0 && (
            <div
              className="owl-carousel owl-full carousel-equal-height carousel-with-shadow"
              data-toggle="owl"
              data-owl-options='{
                                                    "nav": true, 
                                                    "dots": false,
                                                    "margin": 20,
                                                    "loop": false,
                                                    "responsive": {
                                                        "0": {
                                                            "items":2
                                                        },
                                                        "480": {
                                                            "items":2
                                                        },
                                                        "992": {
                                                            "items":3
                                                        },
                                                        "1200": {
                                                            "items":4
                                                        }
                                                    }
                                                }'
            >
              {featureProducts.map((product) => {
                return <ProductCard key={product.id} product={product} />;
              })}
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default FeaturedSection;
