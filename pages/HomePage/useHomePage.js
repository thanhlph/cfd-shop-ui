import { message } from "antd";
import React, { useEffect, useState } from "react";
import { HOME_MESSAGE } from "../../constants/message";
import useMutation from "../../hooks/useMutation";
import useQuery from "../../hooks/useQuery";
import { pageService } from "../../services/pageService";
import { productService } from "../../services/productService";
import { subscribeService } from "../../services/subscribeService";

const useHomePage = () => {
  // API Handling
  const { data: productsData } = useQuery(productService.getProducts);
  const products = productsData?.data?.products;

  const { data: categoriesData } = useQuery(
    productService.getProductCategories
  );
  const categories = categoriesData?.data?.products;

  const { data: homeData } = useQuery(() =>
    pageService.getPageDataByName("home")
  );
  const brands = homeData?.data?.data?.brands || [];
  const services = homeData?.data?.data?.information || {};

  const { execute: dealExecute } = useMutation(subscribeService.subscribeDeal);

  // Modify Data
  const featuredProducts =
    products?.filter((product) => product.featured) || [];

  const onSaleProducts = products?.filter((product) => product.onSale) || [];

  const topRatedProducts =
    products?.filter((product) => product.topRated) || [];

  // Deal Section
  const dealProducts = onSaleProducts?.filter(
    (product) => product.discount > 0
  );
  const dealProps = {
    dealProducts,
  };

  // Intro Section
  const introProducts = featuredProducts?.slice(0, 3);
  const introProps = {
    introProducts,
  };

  // Hot Product Section
  const hotProductsProps = {
    featuredProducts,
    onSaleProducts,
    topRatedProducts,
  };

  // Brands Section
  const brandProps = {
    brands,
  };

  // Featured Section
  const [selectedCateSlug, setSelectedCateSlug] = useState("all");
  const featureProducts =
    selectedCateSlug === "all"
      ? [...(products || [])]
      : products?.filter(
          (product) => product?.category?.slug === selectedCateSlug
        );
  const featuredProps = {
    categories,
    featureProducts,
    selectedCateSlug,
    handleSelectCate: (slug) => setSelectedCateSlug(slug),
  };

  // Services Section
  const serviceProps = {
    services,
  };

  // Get Deal Section
  const handleSubcribeDeal = (email, callback) => {
    if (email) {
      dealExecute(email, {
        onSuccess: (data) => {
          message.success(HOME_MESSAGE.dealSuccess);
          callback?.();
        },
        onFail: (error) => {
          message.error(HOME_MESSAGE.error);
        },
      });
    }
  };

  const getDealProps = {
    handleSubcribeDeal,
  };

  return {
    introProps,
    hotProductsProps,
    dealProps,
    brandProps,
    featuredProps,
    serviceProps,
    getDealProps,
  };
};

export default useHomePage;
