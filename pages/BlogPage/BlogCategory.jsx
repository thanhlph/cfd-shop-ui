import React from "react";
import { Link } from "react-router-dom";

const BlogCategory = () => {
  return (
    <div className="widget widget-cats">
      <h3 className="widget-title">Categories</h3>
      <ul>
        <li>
          <Link to="/">
            Lifestyle <span>3</span>
          </Link>
        </li>
        <li>
          <Link to="/">
            Shopping <span>3</span>
          </Link>
        </li>
        <li>
          <Link to="/">
            Fashion <span>1</span>
          </Link>
        </li>
        <li>
          <Link to="/">
            Travel <span>3</span>
          </Link>
        </li>
        <li>
          <Link to="/">
            Hobbies <span>2</span>
          </Link>
        </li>
      </ul>
    </div>
  );
};

export default BlogCategory;
