import React from "react";
import { Link } from "react-router-dom";

const BlogTag = () => {
  return (
    <div className="widget">
      <h3 className="widget-title">Browse Tags</h3>
      <div className="tagcloud">
        <Link to="/">fashion</Link>
        <Link to="/">style</Link>
        <Link to="/">women</Link>
        <Link to="/">photography</Link>
        <Link to="/">travel</Link>
        <Link to="/">shopping</Link>
        <Link to="/">hobbies</Link>
      </div>
    </div>
  );
};

export default BlogTag;
