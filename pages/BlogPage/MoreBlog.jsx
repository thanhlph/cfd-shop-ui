import React from "react";
import { Link } from "react-router-dom";

const MoreBlog = () => {
  return (
    <div className="widget">
      <h3 className="widget-title">Popular Posts</h3>
      <ul className="posts-list">
        <li>
          <figure>
            <Link to="/">
              <img src="assets/images/blog/sidebar/post-1.jpg" alt="post" />
            </Link>
          </figure>
          <div>
            <span>Nov 22, 2018</span>
            <h4>
              <Link to="/">Aliquam tincidunt mauris eurisus.</Link>
            </h4>
          </div>
        </li>
        <li>
          <figure>
            <Link to="/">
              <img src="assets/images/blog/sidebar/post-2.jpg" alt="post" />
            </Link>
          </figure>
          <div>
            <span>Nov 19, 2018</span>
            <h4>
              <Link to="/">Cras ornare tristique elit.</Link>
            </h4>
          </div>
        </li>
        <li>
          <figure>
            <Link to="/">
              <img src="assets/images/blog/sidebar/post-3.jpg" alt="post" />
            </Link>
          </figure>
          <div>
            <span>Nov 12, 2018</span>
            <h4>
              <Link to="/">Vivamus vestibulum ntulla nec ante.</Link>
            </h4>
          </div>
        </li>
        <li>
          <figure>
            <Link to="/">
              <img src="assets/images/blog/sidebar/post-4.jpg" alt="post" />
            </Link>
          </figure>
          <div>
            <span>Nov 25, 2018</span>
            <h4>
              <Link to="/">Donec quis dui at dolor tempor interdum.</Link>
            </h4>
          </div>
        </li>
      </ul>
    </div>
  );
};

export default MoreBlog;
