import useMutation from "../../hooks/useMutation";
import useQuery from "../../hooks/useQuery";
import { blogService } from "../../services/blogService";

const useBlogPage = () => {
  const {
    data: blogsData,
    loading: blogsLoading,
    error: blogsError,
  } = useQuery(blogService.getBlogList);
  const blogs = blogsData?.data?.blogs;
  console.log("blogs", blogs);
  console.log("blogsLoading :>> ", blogsLoading);
  const blogsListProps = {
    isLoading: blogsLoading,
    isError: !!blogsError,
    blogs,
  };

  return { blogsListProps };
};

export default useBlogPage;
