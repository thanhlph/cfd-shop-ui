import { Skeleton } from "antd";
import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import BlogCard from "../../components/BlogCard";

const BlogSkeletonStyle = styled.div`
  display: flex;
  flex-direction: column;
  gap: 10px;
  padding-bottom: 5%;
`;

const BlogList = ({ blogs, isLoading, isError }) => {
  if ((!isLoading && blogs?.length < 1) || isError) {
    return (
      <div className="entry-container max-col-2" data-layout="fitRows">
        <div className="entry-item col-sm-6">There is no blogs</div>
      </div>
    );
  }
  if (isLoading) {
    return (
      <div className="entry-container max-col-2" data-layout="fitRows">
        <div className="entry-item col-sm-6">
          <article className="entry entry-grid">
            {new Array(6).fill("").map((_, index) => {
              return (
                <BlogSkeletonStyle>
                  <Skeleton.Image
                    active
                    style={{ width: "100%", height: "240" }}
                  />
                  <Skeleton.Input />
                  <Skeleton.Input block />
                </BlogSkeletonStyle>
              );
            })}
          </article>
        </div>
      </div>
    );
  }
  return (
    <div className="entry-container max-col-2" data-layout="fitRows">
      {blogs?.map((blog, index) => {
        return (
          <div key={blog.id || index} className="entry-item col-sm-6">
            <BlogCard blog={blog} />
          </div>
        );
      })}
    </div>
  );
};

export default BlogList;
