import React from "react";
import { Link } from "react-router-dom";
import Breadcrumb from "../../components/Breadcrumb";
import Pagination from "../../components/Pagination";
import { PATHS } from "../../constants/paths";
import BlogAds from "./BlogAds";
import BlogCategory from "./BlogCategory";
import BlogList from "./BlogList";
import BlogSearch from "./BlogSearch";
import BlogTag from "./BlogTag";
import MoreBlog from "./MoreBlog";
import useBlogPage from "./useBlogPage";

const BlogPage = () => {
  const { blogsListProps } = useBlogPage();
  return (
    <>
      <main className="main">
        <div
          className="page-header text-center"
          style={{
            backgroundImage: 'url("assets/images/page-header-bg.jpg")',
          }}
        >
          <div className="container">
            <h1 className="page-title">Blog</h1>
          </div>
        </div>
        <Breadcrumb>
          <Breadcrumb.Item>
            <Link to={PATHS.HOME}>Home</Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item isActive>Blog</Breadcrumb.Item>
        </Breadcrumb>
        <div className="page-content">
          <div className="container">
            <div className="row">
              <div className="col-lg-9">
                <BlogList {...blogsListProps} />
                <Pagination />
              </div>
              <aside className="col-lg-3">
                <div className="sidebar">
                  <BlogSearch />
                  <BlogCategory />
                  <MoreBlog />
                  <BlogAds />
                  <BlogTag />
                </div>
              </aside>
            </div>
          </div>
        </div>
      </main>
    </>
  );
};

export default BlogPage;
