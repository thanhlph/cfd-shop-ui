import queryString from "query-string";
import { useEffect, useMemo } from "react";
import { useLocation, useSearchParams } from "react-router-dom";
import { SORT_OPTIONS } from "../../constants/general";
import useMutation from "../../hooks/useMutation";
import useQuery from "../../hooks/useQuery";
import { productService } from "../../services/productService";

const PRODUCT_LIMIT = 9;

const useProductPage = () => {
  // Initial Hooks
  const { search } = useLocation();
  const queryObject = queryString.parse(search);

  const [_, setSearchParams] = useSearchParams();

  // API Handling
  // const {
  //   data: productsData,
  //   loading: productsLoading,
  //   error: productsError,
  //   refetch: refetchProducts,
  // } = useQuery(
  //   (query) => productService.getProducts(query || `?limit=${PRODUCT_LIMIT}`),
  //   [search],
  //   {
  //     preventInitialCall: true,
  //   }
  // );
  const {
    data: productsData,
    loading: productsLoading,
    error: productsError,
    execute: fetchProducts,
  } = useMutation((query) =>
    productService.getProducts(query || `?limit=${PRODUCT_LIMIT}`)
  );
  const products = productsData?.products || [];
  const productsPagi = productsData?.pagination || {};

  const {
    data: categoriesData,
    loading: categoriesLoading,
    error: categoriesError,
  } = useQuery(productService.getProductCategories);
  const categories = categoriesData?.data?.products || [];

  useEffect(() => {
    // refetchProducts?.(search);
    fetchProducts(search);
  }, [search]);

  // General Function
  const updateQueryString = (queryObject) => {
    const newQueryString = queryString.stringify({
      ...queryObject,
      limit: PRODUCT_LIMIT,
    });
    setSearchParams(new URLSearchParams(newQueryString));
  };

  // Toolbox Props
  const activeSort = useMemo(() => {
    return (
      Object.values(SORT_OPTIONS).find(
        (options) =>
          options.queryObject.orderBy === queryObject.orderBy &&
          options.queryObject.order === queryObject.order
      )?.value || SORT_OPTIONS.popularity.value
    );
  }, [queryObject]);

  const onSortChange = (sortType) => {
    const sortQueryObject = SORT_OPTIONS[sortType].queryObject;
    if (sortQueryObject) {
      updateQueryString({
        ...queryObject,
        ...sortQueryObject,
        page: 1,
      });
    }
  };

  const toolboxProps = {
    showNumb: products?.length || 0,
    totalNumb: productsPagi.total || 0,
    activeSort,
    onSortChange,
  };

  // Product List Props
  const productListProps = {
    isLoading: productsLoading,
    isError: !!productsError,
    products,
  };

  // Product Filter Props
  const onCateFilterChange = (cateId, isChecked) => {
    let newCategoryQuery = Array.isArray(queryObject.category)
      ? [...queryObject.category, cateId]
      : [queryObject.category, cateId];

    if (!isChecked) {
      newCategoryQuery = newCategoryQuery.filter(
        (category) => category !== cateId
      );
    }
    if (!cateId) {
      newCategoryQuery = [];
    }
    updateQueryString({
      ...queryObject,
      category: newCategoryQuery,
      page: 1,
    });
  };

  const handlePriceFilterChange = (values) => {
    if (values?.length === 2) {
      updateQueryString({
        ...queryObject,
        minPrice: values[0],
        maxPrice: values[1],
        page: 1,
      });
    }
  };

  const productFilterProps = {
    categories: categories || [],
    isLoading: categoriesLoading,
    isError: categoriesError,
    activeCategory: Array.isArray(queryObject.category)
      ? queryObject.category
      : [queryObject.category],
    currentPriceRange: [
      queryObject.minPrice || 0,
      queryObject.maxPrice || 1000,
    ],
    onCateFilterChange,
    handlePriceFilterChange,
  };

  // Pagination Props
  const onPagiChange = (page) => {
    updateQueryString({ ...queryObject, page: page });
  };

  const pagiProps = {
    page: Number(productsPagi.page || queryObject.page || 1),
    limit: Number(productsPagi.limit || 0),
    total: Number(productsPagi.total || 0),
    onPagiChange,
  };

  return { productListProps, productFilterProps, pagiProps, toolboxProps };
};

export default useProductPage;
