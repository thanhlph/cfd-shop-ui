import { useEffect, useRef, useState } from "react";

function useDebounce(value, delay) {
  const [debounceValue, setDebounceValue] = useState(value);
  const timeoutRef = useRef(null);

  useEffect(() => {
    if (timeoutRef.current) {
      clearTimeout(timeoutRef.current);
    }
    if (value != true) {
      timeoutRef.current = setTimeout(() => {
        setDebounceValue(value);
      }, delay);
    } else {
      setDebounceValue(value);
    }
    return () => {
      clearTimeout(timeoutRef.current);
    };
  }, [value, delay, timeoutRef]);
  return debounceValue;
}

export default useDebounce;
