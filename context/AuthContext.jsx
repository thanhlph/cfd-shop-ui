import React, { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { createContext } from "react";
import { message } from "antd";
import { authService } from "../services/authService";
import tokenMethod from "../utils/token";
import { PATHS } from "../constants/paths";
import { orderService } from "../services/orderService";
import { productService } from "../services/productService";
import { addressService } from "../services/addressService";

const AuthContext = createContext({});

const AuthContextProvider = ({ children }) => {
  const navigate = useNavigate();
  const [showedModal, setShowedModal] = useState("");
  const [profile, setProfile] = useState({});
  const [product, setProduct] = useState({});
  const [category, setCategory] = useState({});
  const [province, setProvince] = useState({});
  const [ward, setWard] = useState({});
  const [district, setDistrict] = useState({});

  useEffect(() => {
    const accessToken = tokenMethod.get()?.accessToken;

    if (accessToken) {
      handleGetProfile();
      handleGetOrder();
      handleGetProduct();
      handleGetCategories();
      handleGetProvince();
      // handleGetWard();
      // handleGetDistrict();
    }
  }, []);

  // Handle Show Auth Modal
  const handleShowModal = (e, modalType) => {
    e?.stopPropagation();
    e?.preventDefault();
    setShowedModal(modalType || "login");
  };

  // Handle Close Auth Modal
  const handleCloseModal = (e) => {
    e?.stopPropagation();
    e?.preventDefault();
    setShowedModal("");
  };

  // Handle Login
  const handleLogin = async (loginData, callback) => {
    // Xy ly payload
    const payload = { ...loginData };
    // Xu ly APi
    try {
      const res = await authService.login(payload);
      if (res?.data) {
        const { token: accessToken, refreshToken } = res.data.data || {};

        // 1. LocalStorage && 2. Luu = Cookie
        tokenMethod.set({
          accessToken,
          refreshToken,
        });
        handleGetProfile();
        handleCloseModal();
      } else {
        message.error("Đăng nhập thất bại");
      }
    } catch (error) {
      console.log("error", error);
    } finally {
      callback?.();
    }
  };

  // Handle Register
  const handleRegister = async (registerData, callback) => {
    const { email, password } = registerData || {};

    // Xu ly payload
    const payload = {
      firstName: "",
      lastName: "",
      email,
      password,
    };
    try {
      const res = await authService.register(payload);
      if (res?.data) {
        // handle login
        handleLogin({
          email,
          password,
        });
      } else {
        message.error("Đăng ký thất bại");
      }
    } catch (error) {
      console.log("error", error);
      message.error("Đăng ký thất bại");
    } finally {
      callback?.();
    }
  };

  // handle log out
  const handleLogout = () => {
    tokenMethod.remove();
    navigate(PATHS.HOME);
    message.success("Tài khoản đã đăng xuất");
  };

  // Handle Get Profile
  const handleGetProfile = async () => {
    try {
      const res = await authService.getProfile();
      if (res?.data.data) {
        setProfile(res.data.data);
      }
    } catch (error) {
      console.log("error", error);
    }
  };

  // Handle Get Order
  const handleGetOrder = async () => {
    try {
      const res = await orderService.getOrder();
      //   console.log("res :>> ", res);
    } catch (error) {
      console.log("error", error);
    }
  };

  // Handle Get Products
  const handleGetProduct = async () => {
    try {
      const res = await productService.getProducts();
      const products = res.data.data.products;
      setProduct(products);
    } catch (error) {
      console.log("error", error);
    }
  };

  // Handle Get Categories
  const handleGetCategories = async () => {
    try {
      const res = await productService.getProductCategories();
      const categories = res.data.data.products;
      setCategory(categories);
    } catch (error) {
      console.log("error", error);
    }
  };

  // Handle Update Profile
  const handleUpdateProfile = async () => {};

  //Handle Get Province
  const handleGetProvince = async () => {
    try {
      const res = await addressService.getProvince();
      const provinces = res.data.data.provinces;
      setProvince(provinces);
    } catch (error) {
      console.log("error :>> ", error);
    }
  };

  // // Handle Get Ward
  // const handleGetWard = async () => {
  //   try {
  //     const res = await addressService.getWard();
  //     const wards = res.data.data.wards;
  //     setWard(wards);
  //   } catch (error) {
  //     console.log("error :>> ", error);
  //   }
  // };

  // // Handle Get District
  // const handleGetDistrict = async () => {
  //   try {
  //     const res = await addressService.getDistrict();
  //     const districts = res.data.data.districts;
  //     setDistrict(districts);
  //   } catch (error) {
  //     console.log("error :>> ", error);
  //   }
  // };

  return (
    <AuthContext.Provider
      value={{
        profile,
        product,
        category,
        province,
        // ward,
        // district,
        showedModal,
        handleShowModal,
        handleCloseModal,
        handleLogin,
        handleRegister,
        handleLogout,
        handleGetOrder,
        handleGetProduct,
        handleGetCategories,
        handleGetProvince,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export default AuthContextProvider;

export const useAuthContext = () => useContext(AuthContext);
