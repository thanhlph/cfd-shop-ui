import React, { useEffect } from "react";
import { useState } from "react";
import { useContext } from "react";
import { createContext } from "react";
import { useLocation } from "react-router-dom";

const MainContext = createContext();

const MainContextProvider = ({ children }) => {
  const { pathname } = useLocation();

  useEffect(() => {
    handleCloseMobileMenu();

    const myTimeout = setTimeout(() => {
      // scrollToTop
      window.scrollTo({
        top: 0,
        left: 0,
        behavior: "smooth",
      });
    }, 100);

    return () => {
      clearTimeout(myTimeout);
    };
  }, [pathname]);

  const handleShowMobileMenu = (e) => {
    e?.stopPropagation();
    e?.preventDefault();
    $("body").addClass("mmenu-active");
  };

  const handleCloseMobileMenu = (e) => {
    e?.stopPropagation();
    e?.preventDefault();
    $("body").removeClass("mmenu-active");
  };

  return (
    <MainContext.Provider
      value={{ handleShowMobileMenu, handleCloseMobileMenu }}
    >
      {children}
    </MainContext.Provider>
  );
};

export default MainContextProvider;

export const useMainContext = () => useContext(MainContext);
