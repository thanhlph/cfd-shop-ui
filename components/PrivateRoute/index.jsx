import React, { useEffect } from "react";
import { Navigate, Outlet, redirect, useNavigate } from "react-router-dom";
import { MODAL_TYPE } from "../../constants/general";
import { useMainContext } from "../../context/MainContext";
import tokenMethod from "../../utils/token";

const PrivateRoute = () => {
  const { handleShowModal } = useMainContext();
  const navigate = useNavigate();

  useEffect(() => {
    if (!tokenMethod.get()) {
      handleShowModal?.(MODAL_TYPE.login);
    }
  }, [handleShowModal]);

  if (!tokenMethod.get()) {
    if (redirectPath) {
      return <Navigate to={redirectPath} />;
    } else {
      navigate(-1);
    }
  }
  return <Outlet />;
};

export default PrivateRoute;
