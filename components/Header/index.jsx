import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import HeaderMiddle from "./HeaderMiddle";
import HeaderTop from "./HeaderTop";

const Header = () => {
  return (
    <header className="header">
      <HeaderTop />
      <HeaderMiddle />
    </header>
  );
};

export default Header;
