import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import { MODAL_TYPE } from "../../constants/general";
import { PATHS } from "../../constants/paths";
import { useAuthContext } from "../../context/AuthContext";
import {
  handleGetProfile,
  handleLogout,
  handleShowModal,
} from "../../stores/reducer/authReducer";
import tokenMethod from "../../utils/token";

const HeaderTop = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { profile, loading } = useSelector((state) => state.auth);
  const { firstName, email } = profile || {};

  useEffect(() => {
    dispatch(handleGetProfile());
  }, []);

  const _onShowAuthModal = (e) => {
    e?.preventDefault();
    e?.stopPropagation();
    dispatch(handleShowModal(MODAL_TYPE.login));
  };

  const _onSignOut = (e) => {
    e?.preventDefault();
    dispatch(handleLogout());
    navigate(PATHS.HOME);
  };

  return (
    <div className="header-top">
      <div className="container">
        <div className="header-left">
          <a href="tel:0989596912">
            <i className="icon-phone" /> Hotline: 098 9596 912{" "}
          </a>
        </div>
        <div className="header-right">
          {tokenMethod.get() ? (
            <ul className="top-menu">
              <li>
                <Link to="/" className="top-link-menu">
                  <i className="icon-user" />
                  {firstName || email}
                </Link>
                <ul>
                  <li>
                    <ul>
                      <li>
                        <Link to="/dashboard">Account Details</Link>
                      </li>
                      <li>
                        <Link to="/dashboard">Your Orders</Link>
                      </li>
                      <li>
                        <Link to="/dashboard">
                          Wishlist <span>(3)</span>
                        </Link>
                      </li>
                      <li>
                        <Link onClick={_onSignOut} to="/">
                          Sign Out
                        </Link>
                      </li>
                    </ul>
                  </li>
                </ul>
              </li>
            </ul>
          ) : (
            <ul class="top-menu top-link-menu">
              <li>
                <a
                  href="#signin-modal"
                  data-toggle="modal"
                  class="top-menu-login"
                  onClick={_onShowAuthModal}
                >
                  <i class="icon-user"></i>
                  Login | Resgister{" "}
                </a>
              </li>
            </ul>
          )}
        </div>
      </div>
    </div>
  );
};

export default HeaderTop;
