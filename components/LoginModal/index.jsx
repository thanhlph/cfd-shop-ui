import React, { useState } from "react";
import { MODAL_TYPE } from "../../constants/general.js";
import LoginForm from "./LoginForm";
import RegisterForm from "./RegisterForm";
import { useDispatch, useSelector } from "react-redux";
import {
  handleCloseModal,
  handleShowModal,
} from "../../stores/reducer/authReducer.js";

const LoginModal = () => {
  const { showedModal } = useSelector((state) => state.auth);
  const dispatch = useDispatch();

  const _onTabChange = (e, tab) => {
    e?.preventDefault();
    e?.stopPropagation();
    dispatch(handleShowModal(tab));
  };

  const _onCloseModal = (e) => {
    e?.preventDefault();
    e?.stopPropagation();
    dispatch(handleCloseModal());
  };

  return (
    <>
      <div
        className={`modal ${showedModal ? "fade show" : ""}`}
        id="signin-modal"
        tabIndex={-1}
        role="dialog"
        aria-hidden="true"
        style={{ display: showedModal ? "block" : "none" }}
      >
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content">
            <div className="modal-body">
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
                onClick={_onCloseModal}
              >
                <span aria-hidden="true">
                  <i className="icon-close" />
                </span>
              </button>
              <div className="form-box">
                <div className="form-tab">
                  <ul
                    className="nav nav-pills nav-fill nav-border-anim"
                    role="tablist"
                  >
                    <li className="nav-item active">
                      <a
                        className={`nav-link ${
                          showedModal === MODAL_TYPE.login ? "active" : ""
                        }`}
                        id="signin-tab"
                        data-toggle="tab"
                        href="#signin"
                        role="tab"
                        aria-controls="signin"
                        aria-selected="true"
                        onClick={(e) => _onTabChange(e, MODAL_TYPE.login)}
                      >
                        Sign In
                      </a>
                    </li>
                    <li className="nav-item">
                      <a
                        className={`nav-link ${
                          showedModal === MODAL_TYPE.register ? "active" : ""
                        }`}
                        id="register-tab"
                        data-toggle="tab"
                        href="#register"
                        role="tab"
                        aria-controls="register"
                        aria-selected="false"
                        onClick={(e) => _onTabChange(e, MODAL_TYPE.register)}
                      >
                        Register
                      </a>
                    </li>
                  </ul>
                  <div className="tab-content" id="tab-content-5">
                    {showedModal === MODAL_TYPE.login && (
                      <div
                        className="tab-pane fade show active"
                        id="signin"
                        role="tabpanel"
                        aria-labelledby="signin-tab"
                      >
                        <LoginForm />

                        {/* End .form-choice */}
                      </div>
                    )}
                    {/* .End .tab-pane */}
                    {showedModal === MODAL_TYPE.register && (
                      <div
                        className="tab-pane fade show active"
                        id="register"
                        role="tabpanel"
                        aria-labelledby="register-tab"
                      >
                        <RegisterForm />

                        {/* End .form-choice */}
                      </div>
                    )}
                    {/* .End .tab-pane */}
                  </div>
                  {/* End .tab-content */}
                </div>
                {/* End .form-tab */}
              </div>
              {/* End .form-box */}
            </div>
            {/* End .modal-body */}
          </div>
          {/* End .modal-content */}
        </div>
        {/* End .modal-dialog */}
      </div>
      {!!showedModal && (
        <div className="modal-backdrop fade show" onClick={_onCloseModal}></div>
      )}
    </>
  );
};

export default LoginModal;
