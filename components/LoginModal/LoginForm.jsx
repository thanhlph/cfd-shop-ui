import React from "react";
import { useState } from "react";
import { message } from "antd";
import { useAuthContext } from "../../context/AuthContext";
import useForm from "../../hooks/useForm";
import { regexRule, requireRule } from "../../utils/validate";
import ComponentLoading from "../ComponentLoading";
import Input from "../Input";
import { useDispatch, useSelector } from "react-redux";
import {
  handleGetProfile,
  handleLogin,
} from "../../stores/reducer/authReducer";

const LoginForm = () => {
  const dispatch = useDispatch();
  const { loading } = useSelector((state) => state.auth);
  // const { handleLogin, handleCloseModal } = useAuthContext();
  const { form, register, validate } = useForm(
    {
      email: "",
      password: "",
    },
    {
      email: [
        requireRule("Vui lòng nhập email"),
        regexRule("Vui lòng nhập đúng định dạng email", "email"),
      ],
      password: [requireRule("Vui lòng nhập mật khẩu")],
    }
  );

  const _onSubmit = (e) => {
    e.preventDefault();
    const errorObject = validate();
    if (Object.keys(errorObject).length > 0) {
      console.log("Submit error :>> ", errorObject);
    } else {
      // handleLogin?.(form, () => {
      //   setTimeout(() => {
      //     setLoading(false);
      //   }, 300);
      // });
      const res = dispatch(handleLogin(form));
    }
  };

  return (
    <>
      {loading.login && <ComponentLoading />}
      <form onSubmit={_onSubmit}>
        <Input
          type="text"
          id="singin-email"
          label="Email *"
          placeholder="Your email address"
          required
          {...register("email")}
        />
        <Input
          type="password"
          id="singin-password"
          label="Password *"
          placeholder="Your password"
          required
          {...register("password")}
        />
        {/* <label htmlFor="singin-email">Your email address *</label>
          <input
            type="text"
            className="form-control input-error"
            id="singin-email"
            name="singin-email"
            required
          />
          <p className="form-error">Please fill in this field</p> */}

        {/* End .form-group */}
        {/* <div className="form-group">
          <label htmlFor="singin-password">Password *</label>
          <input
            type="password"
            className="form-control"
            id="singin-password"
            name="singin-password"
            required
          />
        </div> */}
        {/* End .form-group */}
        <div className="form-footer">
          <button type="submit" className="btn btn-outline-primary-2">
            <span>LOG IN</span>
            <i className="icon-long-arrow-right" />
          </button>
        </div>
        {/* End .form-footer */}
      </form>
    </>
  );
};

export default LoginForm;
