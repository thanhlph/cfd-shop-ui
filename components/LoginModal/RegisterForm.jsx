import React from "react";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { useAuthContext } from "../../context/AuthContext";
import useForm from "../../hooks/useForm";
import { regexRule, requireRule } from "../../utils/validate";
import ComponentLoading from "../ComponentLoading";
import Input from "../Input";
import { handleRegister } from "../../stores/reducer/authReducer";

const RegisterForm = () => {
  const [loading, setLoading] = useState(false);
  // const { handleRegister } = useAuthContext();
  const dispatch = useDispatch();

  const { form, register, validate } = useForm(
    {
      email: "",
      password: "",
    },
    {
      email: [
        requireRule("Vui lòng nhập email"),
        regexRule("Vui lòng nhập đúng định dạng email", "email"),
      ],
      password: [requireRule("Vui lòng nhập mật khẩu")],
    }
  );

  const _onSubmit = async (e) => {
    e.preventDefault();
    const errorObject = validate();
    if (Object.keys(errorObject).length > 0) {
      console.log("Submit error :>> ", errorObject);
    } else {
      setLoading(true);
      const { name, email, password } = form;
      const payload = {
        firstName: name || "",
        lastName: "",
        email,
        password,
      };
      try {
        const res = await dispatch(handleRegister(payload)).unwrap();
      } catch (error) {
      } finally {
        setTimeout(() => {
          setLoading(false);
        }, 300);
      }

      // handleRegister?.(form, () => {
      // });
    }
  };

  return (
    <>
      {loading && <ComponentLoading />}
      <form onSubmit={_onSubmit}>
        <Input
          type="text"
          id="singin-email"
          label="Email *"
          placeholder="Your email address"
          required
          {...register("email")}
        />
        <Input
          type="password"
          id="singin-password"
          label="Password *"
          placeholder="Your password"
          required
          {...register("password")}
        />
        {/* <div className="form-group">
        <label htmlFor="register-email">Your email address *</label>
        <input
          type="email"
          className="form-control input-error"
          id="register-email"
          name="register-email"
          required
        />
        <p className="form-error">Please fill in this field</p>
      </div> */}
        {/* End .form-group */}
        {/* <div className="form-group">
        <label htmlFor="register-password">Password *</label>
        <input
          type="password"
          className="form-control"
          id="register-password"
          name="register-password"
          required
        />
      </div> */}
        {/* End .form-group */}
        <div className="form-footer">
          <button type="submit" className="btn btn-outline-primary-2">
            <span>SIGN UP</span>
            <i className="icon-long-arrow-right" />
          </button>
          <div className="custom-control custom-checkbox">
            <input
              type="checkbox"
              className="custom-control-input"
              id="register-policy"
              required
            />
            <label className="custom-control-label" htmlFor="register-policy">
              I agree to the
              <a href="privacy-policy.html">privacy policy</a> *
            </label>
          </div>
          {/* End .custom-checkbox */}
        </div>
        {/* End .form-footer */}
      </form>
    </>
  );
};

export default RegisterForm;
