import React from "react";

const Select = ({ options, error, ...rest }) => {
  return (
    <select
      className="form-control form-select select-custom"
      aria-label="Default select example"
      {...rest}
    >
      {options?.map((option, index) => {
        return (
          <option key={option?.value || index} value={option?.value}>
            {option?.label || ""}
          </option>
        );
      })}
    </select>
  );
};

export default Select;
