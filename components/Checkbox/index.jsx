import React from "react";

const Checkbox = ({ id, label, onChange, className, ...props }) => {
  return (
    <div className="custom-control custom-checkbox">
      <input
        className="custom-control-input"
        type="checkbox"
        id={id || "checkbox"}
        onChange={onChange}
        {...props}
      />
      <label className="custom-control-label" htmlFor={id || "checkbox"}>
        {label}
      </label>
    </div>
  );
};

export default Checkbox;
