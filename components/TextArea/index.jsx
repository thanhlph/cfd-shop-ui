import React from "react";

const TextArea = ({ error, ...rest }) => {
  return <textarea type="text" {...rest} className="form-control" />;
};

export default TextArea;
