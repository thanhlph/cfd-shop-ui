import React from "react";
import { Link } from "react-router-dom";

const FooterMiddle = () => {
  return (
    <div className="footer-middle">
      <div className="container">
        <div className="row">
          <div className="col-sm-6 col-lg-5">
            <div className="widget widget-about">
              <img
                src="assets/images/logo.svg"
                className="footer-logo"
                alt="Footer Logo"
                width={120}
              />
              <p>
                Praesent dapibus, neque id cursus ucibus, tortor neque egestas
                augue, eu vulputate magna eros eu erat.{" "}
              </p>
              <div className="widget-call">
                <i className="icon-phone" /> Got Question? Call us 24/7{" "}
                <a href="tel:#">098 9596 912</a>
              </div>
            </div>
          </div>
          <div className="col-sm-6 col-lg-2 offset-lg-1">
            <div className="widget">
              <h4 className="widget-title">Useful Links</h4>
              <ul className="widget-list">
                <li>
                  <Link to="/about">About Us</Link>
                </li>
                <li>
                  <Link to="/product">Product</Link>
                </li>
                <li>
                  <Link to="/faq">FAQs</Link>
                </li>
                <li>
                  <Link to="/contact">Contact us</Link>
                </li>
              </ul>
            </div>
          </div>
          <div className="col-sm-6 col-lg-2">
            <div className="widget">
              <h4 className="widget-title">Customer Service</h4>
              <ul className="widget-list">
                <li>
                  <Link to="/payment-methods">Payment Methods</Link>
                </li>
                <li>
                  <Link to="/returns">Returns</Link>
                </li>
                <li>
                  <Link to="/shipping">Shipping</Link>
                </li>
                <li>
                  <Link to="/privacy-policy">Privacy Policy</Link>
                </li>
              </ul>
            </div>
          </div>
          <div className="col-sm-6 col-lg-2">
            <div className="widget">
              <h4 className="widget-title">My Account</h4>
              <ul className="widget-list">
                <li>
                  <Link to="/dashboard">Account Details</Link>
                </li>
                <li>
                  <Link to="/cart">View Cart</Link>
                </li>
                <li>
                  <Link to="/dashboard">My Wishlist</Link>
                </li>
                <li>
                  <Link to="/dashboard">Track My Order</Link>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FooterMiddle;
