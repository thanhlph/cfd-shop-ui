import React from "react";
import FooterBottom from "./FooterBottom";
import FooterMiddle from "./FooterMiddle";

const Footer = () => {
  return (
    <footer className="footer">
      <FooterMiddle />
      <FooterBottom />
    </footer>
  );
};

export default Footer;
