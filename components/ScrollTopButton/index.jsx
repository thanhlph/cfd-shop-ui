import React from "react";

const ScrollTopButton = () => {
  return (
    <button id="scroll-top" title="Back to Top">
      <i className="icon-arrow-up" />
    </button>
  );
};

export default ScrollTopButton;
