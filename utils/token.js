import Cookies from "js-cookie";
import { STORAGE } from "../constants/storage";

// Local storage token
// export const localToken = {
//   get: () => JSON.parse(localStorage.get(STORAGE.token)),
//   set: () => localStorage.setItem(STORAGE.token, JSON.stringify(token)),
//   remove: () => localStorage.removeItem(STORAGE.token),
// };

// Cookie Token
export const cookieToken = {
  get: () =>
    JSON.parse(
      Cookies.get(STORAGE.token) !== undefined
        ? Cookies.get(STORAGE.token)
        : null
    ),
  set: (token) => Cookies.set(STORAGE.token, JSON.stringify(token)),
  remove: () => Cookies.remove(STORAGE.token),
};

const tokenMethod = {
  get: () => {
    return cookieToken.get();
  },
  set: (token) => {
    return cookieToken.set(token);
  },
  remove: () => {
    return cookieToken.remove();
  },
};

export default tokenMethod;
