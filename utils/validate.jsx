export const REGEX = {
  email: /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/,
  phone: /(84|0[3|5|7|8|9])+([0-9]{8})\b/,
};

export const MESSAGE = {
  required: "Vui lòng nhập",
  email: "Vui lòng nhập đúng định dạng email",
  phone: "Vui lòng nhập đúng định dạng phone",
};

const validate = (rules, values) => {
  let errorObj = {};

  for (const ruleKey in rules) {
    for (const rule of rules[ruleKey]) {
      // Case Function
      if (typeof rule === "function") {
        const message = rule(values[ruleKey], values);

        if (message) {
          errorObj[ruleKey] = message || "Xác thực lỗi";
          break;
        }
      }
      // Case required
      if (rule.required) {
        // Check Required
        if (!values[ruleKey]) {
          errorObj[ruleKey] = rule.message || "Vui lòng nhập";
          break;
        }
      }
      // Case Regex
      if (rule.regex instanceof RegExp) {
        // Check regex
        if (!rule.regex.test(values[ruleKey])) {
          errorObj[ruleKey] = rule.message || "Vui lòng nhập đúng định dạng";
          break;
        }
      } else if (rule.regex in REGEX) {
        if (!REGEX[rule.regex].test(values[ruleKey])) {
          errorObj[ruleKey] = rule.message || "Vui lòng nhập đúng định dạng";
          break;
        }
      }
    }
  }
  return errorObj;
};

export const requireRule = (message) => {
  return {
    required: true,
    message,
  };
};

export const regexRule = (message, regex) => {
  return {
    regex,
    message,
  };
};

export default validate;
