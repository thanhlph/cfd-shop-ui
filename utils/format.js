import moment from "moment";

export const formatCurrency = (data, type = "vi-VN") => {
  if (!data || isNaN(data)) return 0;
  return data.toLocaleString(type);
};
export const transformNumberToPercent = (number) => {
  if (!number) return 0;
  return number * 100;
};

export const formatDate = (date, format = "DD/MM/YYYY") => {
  if (!date) {
    return "";
  }
  return moment(date).format(format);
};
