import axiosInstance from "../utils/axiosInstance";

export const productService = {
  getProducts(query = "") {
    return axiosInstance.get(`/products${query}`);
  },
  getProductBySlug(slug = "") {
    return axiosInstance.get(`/products/${slug}`);
  },
  getProductCategories(query = "") {
    return axiosInstance.get(`/product-categories${query}`);
  },
  getProductByCategory(slug = "") {
    return axiosInstance.get(`/product-categories/${slug}`);
  },
  getProductReview(id = "", query = "") {
    return axiosInstance.get(`/reviews/product/${id}${query}`);
  },
};
