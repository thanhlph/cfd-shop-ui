import axiosInstance from "../utils/axiosInstance";

export const orderService = {
  getVoucher(code = "") {
    return axiosInstance.get(`/orders/voucher/${code}`);
  },
  getOrder() {
    return axiosInstance.get(`/orders/me`);
  },
  getOrderById(id = "") {
    return axiosInstance.get(`/orders/${id}/me`);
  },
  orderCheckOut() {
    return axiosInstance.post(`/orders`);
  },
};
