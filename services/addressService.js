import axiosInstance from "../utils/axiosInstance";

export const addressService = {
  getProvince() {
    return axiosInstance.get(`/provinces`);
  },
  getProvinceById(id = "") {
    return axiosInstance.get(`/provinces/${id}`);
  },
  getDistrict() {
    return axiosInstance.get(`/districts`);
  },
  getDistrictById(id = "") {
    return axiosInstance.get(`/districts/${id}`);
  },
  getWard() {
    return axiosInstance.get(`/wards`);
  },
  getWardById(id = "") {
    return axiosInstance.get(`/wards/${id}`);
  },
};
