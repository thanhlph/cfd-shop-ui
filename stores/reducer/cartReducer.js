import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { message } from "antd";
import { MODAL_TYPE } from "../../constants/general";
import { cartService } from "../../services/cartService";
import { sumArrayNumber } from "../../utils/calculate";
import tokenMethod from "../../utils/token";
import { handleLogin, handleShowModal } from "./authReducer";

const initialState = {
  cartInfo: [],
  cartLoading: false,
};

export const cartSlice = createSlice({
  initialState,
  name: "cart",
  reducers: {
    updateCacheCart: (state, action) => {
      state.cartInfo = action.payload || state.cartInfo;
    },
    clearCart: (state) => {
      state.cartInfo = {};
    },
  },
  extraReducers: (builder) => {
    // Get Cart
    builder.addCase(handleGetCart.pending, (state) => {
      state.cartLoading = true;
    });
    builder.addCase(handleGetCart.fulfilled, (state, action) => {
      state.cartLoading = false;
      state.cartInfo = action.payload;
    });
    builder.addCase(handleGetCart.rejected, (state) => {
      state.cartLoading = false;
      state.cartInfo = {};
    });
    // Remove Cart
    builder.addCase(handleRemoveFromCart.pending, (state) => {
      state.cartLoading = true;
    });
    builder.addCase(handleRemoveFromCart.fulfilled, (state) => {
      state.cartLoading = false;
    });
    builder.addCase(handleRemoveFromCart.rejected, (state) => {
      state.cartLoading = false;
    });
    // Add Cart
    builder.addCase(handleAddCart.pending, (state) => {
      state.cartLoading = true;
    });
    builder.addCase(handleAddCart.fulfilled, (state) => {
      state.cartLoading = false;
    });
    builder.addCase(handleAddCart.rejected, (state) => {
      state.cartLoading = false;
    });
  },
});

const { actions, reducer: cartReducer } = cartSlice;
export const { updateCacheCart, clearCart } = actions;
export default cartReducer;

export const handleGetCart = createAsyncThunk(
  "cart/get",
  async (_, thunkApi) => {
    try {
      const cartRes = await cartService.getCart();
      return cartRes.data?.data;
    } catch (error) {
      thunkApi.rejectWithValue(error);
    }
  }
);

export const handleAddCart = createAsyncThunk(
  "cart/add",
  async (actionPayload, thunkApi) => {
    try {
      const { addedId, addedColor, addedQuantity, addedPrice } = actionPayload;
      const { cartInfo } = thunkApi.getState()?.cart || {};

      let addPayload = {};

      if (cartInfo?.id) {
        const matchIndex = cartInfo.product?.findIndex(
          (product, index) =>
            product.id === addedId && cartInfo.variant[index] === addedColor
        );
        const newProduct = cartInfo.product?.map((product) => {
          return product.id;
        });
        const newQuantity = [...(cartInfo.quantity ?? [])];
        const newVariant = [...(cartInfo.variant ?? [])];
        const newTotalProduct = [...(cartInfo.totalProduct ?? [])];

        if (matchIndex > -1) {
          newQuantity[matchIndex] =
            Number(newQuantity[matchIndex]) + Number(addedQuantity);
          // newVariant[matchIndex] = addedColor;
          newTotalProduct[matchIndex] =
            Number(newTotalProduct[matchIndex]) + addedPrice * addedQuantity;
        } else {
          newProduct.push(addedId);
          newQuantity.push(addedQuantity);
          newVariant.push(addedColor);
          newTotalProduct.push(addedPrice * addedQuantity);
        }
        const newSubtotal =
          newTotalProduct.reduce(
            (curr, next) => Number(curr) + Number(next),
            0
          ) || 0;
        const newTotal = newSubtotal - cartInfo.discount;

        addPayload = {
          ...cartInfo,
          product: newProduct,
          quantity: newQuantity,
          variant: newVariant,
          subTotal: newSubtotal,
          total: newTotal,
          totalProduct: newTotalProduct,
        };
      } else {
        addPayload = {
          product: [addedId],
          quantity: [addedQuantity],
          variant: [addedColor],
          totalProduct: [addedPrice * addedQuantity],
          subTotal: addedPrice * addedQuantity,
          total: addedPrice * addedQuantity,
          discount: 0,
          paymentMethod: "",
        };
      }

      const cartRes = await cartService.updateCart(addPayload);
      thunkApi.dispatch(handleGetCart());
      message.success("Add to cart successfully!");
      return cartRes?.data?.data;
    } catch (error) {
      thunkApi.rejectWithValue(error);
      message.error("Add to cart failed");
    }
  }
);

export const handleRemoveFromCart = createAsyncThunk(
  "cart/removeProduct",
  async (actionPayload, thunkApi) => {
    const { removedIndex } = actionPayload || {};
    const { dispatch, getState, rejectWithValue } = thunkApi;
    const { cartInfo } = getState()?.cart || {};

    if (removedIndex < 0) return false;

    try {
      const newProduct = cartInfo.product
        ?.filter((_, index) => index !== removedIndex)
        .map((item) => item.id);
      const newQuantity = cartInfo.quantity?.filter(
        (_, index) => index !== removedIndex
      );
      const newVariant = cartInfo.variant?.filter(
        (_, index) => index !== removedIndex
      );
      const newTotalProduct = cartInfo.totalProduct?.filter(
        (_, index) => index !== removedIndex
      );
      const newSubtotal = sumArrayNumber(newTotalProduct);
      const newTotal =
        newSubtotal -
        (cartInfo.discount ?? 0) +
        (cartInfo.shipping?.price ?? 0);

      const updatePayload = {
        ...cartInfo,
        product: newProduct,
        quantity: newQuantity,
        variant: newVariant,
        totalProduct: newTotalProduct,
        subTotal: newSubtotal,
        total: newTotal,
        shipping: newProduct?.length > 0 ? cartInfo.shipping : {},
        discount: newProduct?.length > 0 ? cartInfo.discount : 0,
      };
      const cartRes = await cartService.updateCart(updatePayload);
      dispatch(handleGetCart());
      message.success("Remove from cart successfully");
      return cartRes?.data.data;
    } catch (error) {
      rejectWithValue(error);
      message.error("Remove from cart failed");
      console.log("error :>> ", error);
    }
  }
);
