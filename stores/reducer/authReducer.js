import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { message } from "antd";
import tokenMethod from "../../utils/token";
import { authService } from "../../services/authService";
import { handleGetCart } from "./cartReducer";

const initialState = {
  showedModal: "",
  profile: null,
  loading: {
    login: false,
    register: false,
    getProfile: false,
  },
};

export const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    handleShowModal: (state, action) => {
      state.showedModal = action.payload;
    },
    handleCloseModal: (state) => {
      state.showedModal = "";
    },
    handleLogout: (state) => {
      tokenMethod.remove();
      state.profile = null;
      state.showedModal = "";
      message.success("Đăng xuất thành công");
    },
  },
  extraReducers: (builder) => {
    // Handle Get Profile
    builder
      .addCase(handleGetProfile.fulfilled, (state, action) => {
        state.profile = action.payload;
        state.loading.getProfile = false;
      })
      .addCase(handleGetProfile.pending, (state) => {
        state.loading.getProfile = true;
      })
      .addCase(handleGetProfile.rejected, (state) => {
        state.loading.getProfile = false;
      })
      // Handle Login
      .addCase(handleLogin.fulfilled, (state) => {
        state.loading.login = false;
        state.showedModal = "";
      })
      .addCase(handleLogin.pending, (state) => {
        state.loading.login = true;
      })
      .addCase(handleLogin.rejected, (state) => {
        state.loading.login = false;
      })
      // Handle Register
      .addCase(handleRegister.fulfilled, (state) => {
        state.loading.register = false;
      })
      .addCase(handleRegister.pending, (state) => {
        state.loading.register = true;
      })
      .addCase(handleRegister.rejected, (state) => {
        state.loading.register = false;
      });
  },
});

const { actions, reducer: authReducer } = authSlice;
export const { handleLogout, handleShowModal, handleCloseModal } = actions;
export default authReducer;

export const handleLogin = createAsyncThunk(
  "auth/handleLogin",
  async (payload, thunkApi) => {
    try {
      const res = await authService.login(payload);
      if (res?.data) {
        const { token: accessToken, refreshToken } = res.data.data || {};

        // 1. LocalStorage && 2. Luu = Cookie
        tokenMethod.set({
          accessToken,
          refreshToken,
        });
        thunkApi.dispatch(handleGetProfile());
        thunkApi.dispatch(handleGetCart());
        message.success("Đăng nhập thành công");
        // handleCloseModal();
        // thunkApi.dispatch(handleCloseModal());

        return true;
      }
    } catch (error) {
      const errorInfo = error?.response?.data;
      if (errorInfo.error === "Not Found") {
        message.error("Email hoặc Password không đúng");
      }
      return thunkApi.rejectWithValue(errorInfo);
    }
  }
);

export const handleRegister = createAsyncThunk(
  "auth/handleRegister",
  async (payload, thunkApi) => {
    try {
      const registerRes = await authService.register(payload);
      if (registerRes?.data?.data?.id) {
        message.success("Đăng ký thành công");
        thunkApi.dispatch(
          handleLogin({
            email: payload.email,
            password: payload.password,
          })
        );
        return true;
      } else {
        throw false;
      }
    } catch (error) {
      const errorInfo = error?.response?.data;
      if (errorInfo.error === "Forbidden") {
        message.error("Email đã được đăng ký");
      }
      return thunkApi.rejectWithValue(errorInfo);
    }
  }
);

export const handleGetProfile = createAsyncThunk(
  "auth/handleGetProfile",
  async (_, thunkApi) => {
    if (tokenMethod.get()) {
      try {
        const profileRes = await authService.getProfile();
        return profileRes?.data?.data;
      } catch (error) {
        return thunkApi.rejectWithValue(error?.response?.data);
      }
    }
  }
);
