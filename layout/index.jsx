import React from "react";
import { Outlet } from "react-router-dom";
import Footer from "../components/Footer";
import Header from "../components/Header";
import LoginModal from "../components/LoginModal";
import MobileMenu from "../components/MobileMenu";
import ScrollTopButton from "../components/ScrollTopButton";
import AuthContextProvider from "../context/AuthContext";
import MainContextProvider from "../context/MainContext";

const MainLayout = () => {
  return (
    <MainContextProvider>
      {/* <AuthContextProvider> */}
      <div className="page-wrapper">
        {/* Header */}
        <Header />

        {/* Main */}
        <Outlet />

        {/* Footer */}
        <Footer />
      </div>

      <ScrollTopButton />
      {/* Mobile Menu */}
      <MobileMenu />
      {/* End .mobile-menu-container */}
      {/* Sign in / Register Modal */}
      <LoginModal />
      {/* End .modal */}
      {/* </AuthContextProvider> */}
    </MainContextProvider>
  );
};

export default MainLayout;
